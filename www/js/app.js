// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.cloud', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform,AppVersion) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
      // cordova.getAppVersion(function(version){
      //     AppVersion.set(version);
      //     console.log(version);
      // });
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
  });
})

.directive('input', function($timeout) {
  return {
    restrict: 'E',
    scope: {
      'returnClose': '=',
      'onReturn': '&',
      'onFocus': '&',
      'onBlur': '&'
    },
    link: function(scope, element, attr) {
      element.bind('focus', function(e) {
        if (scope.onFocus) {
          $timeout(function() {
            scope.onFocus();
          });
        }
      });
      element.bind('blur', function(e) {
        if (scope.onBlur) {
          $timeout(function() {
            scope.onBlur();
          });
        }
      });
      element.bind('keydown', function(e) {
        if (e.which == 13) {
          if (scope.returnClose) element[0].blur();
          if (scope.onReturn) {
            $timeout(function() {
              scope.onReturn();
            });
          }
        }
      });
    }
  }
})

.config(function($stateProvider, $urlRouterProvider,$ionicCloudProvider) {
  //cloud
  $ionicCloudProvider.init({
    "core": {
      "app_id": "e379eb88"
    },
    "push": {
      "sender_id": "975040239797",
      "pluginConfig": {
        "ios": {
          "badge": true,
          "sound": true
        },
        "android": {
          "iconColor": "#343434"
        }
      }
    }
  });
  

  $stateProvider
  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'DashCtrl'
  })
  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })
  .state('tab.new', {
    url: '/new',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-new.html',
        controller: 'DashCtrl'
      }
    }
  })
  .state('tab.inprocess', {
    url: '/inprocess',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-inprocess.html',
        controller: 'DashCtrl'
      }
    }
  })
  .state('tab.rejected', {
    url: '/rejected',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-rejected.html',
        controller: 'DashCtrl'
      }
    }
  })
  .state('tab.total', {
    url: '/total',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-total.html',
        controller: 'DashCtrl'
      }
    }
  })
  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-dash': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
    .state('tab.product', {
      url: '/product',
      views: {
        'tab-dash': {
          templateUrl: 'templates/page-product.html',
          controller: 'AccountCtrl'
        }
      }
    })
  .state('tab.account', {
    url: '/account',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });
  $urlRouterProvider.otherwise('/login');
  
});
