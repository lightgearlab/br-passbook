angular.module('starter.controllers', ['ionic','ionic.cloud'])

.controller('DashCtrl', function($scope,$window,$stateParams,$ionicModal,$http,$httpExtended,$location,sharedToken,$interval,$ionicLoading,$ionicDeploy) {
  $scope.fa_name = sharedToken.getname();
  var refreshList = function() {
      $http.get("https://brmarketplace.com.my/apis/applications?auth_token="+sharedToken.gettoken()).then(function(r){
      //console.log(r);
      //console.log(sharedToken.gettoken());
      // -99 ->rejected   ; 0 -> new;20 submitted/in process; 80 approved ; 100 delivered ; 
      if(r.data.success){
        $scope.total = r.data.applications;
        $scope.requests = r.data.applications;
        $scope.new = $scope.requests.filter(function (request) {
            return (request.status == 0 && request.user.name != null);
        });
        $scope.inprocess = $scope.requests.filter(function (request) {
            return (request.status == 20 && request.user.name != null);
        });
        $scope.rejected = $scope.requests.filter(function (request) {
            return (request.status == -99 && request.user.name != null);
        });
        $scope.onClickTab = function (tab) {
          $scope.requests = tab;
          if(tab == $scope.new){
            $scope.tab_new = "active";
            $scope.tab_inprocess = "";
            $scope.tab_rejected = "";
          }else if(tab == $scope.inprocess){
            $scope.tab_new = "";
            $scope.tab_inprocess = "active";
            $scope.tab_rejected = "";
          }
          else if(tab == $scope.rejected){
            $scope.tab_new = "";
            $scope.tab_inprocess = "";
            $scope.tab_rejected = "active";
          }
        }
        //default
        $scope.requests = $scope.new;
        $scope.tab_new = "active";
        $scope.tab_inprocess = "";
        $scope.tab_rejected = "";
      }else{
        $window.location.href = "#/login";
        $window.location.reload();
      }
    });
  };
  $ionicModal.fromTemplateUrl('templates/my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function(request) {
    $scope.request = request;
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  $scope.gotoChat = function(id) {
    //$location.path('/tab/chats/'+id);
    //console.log(id);
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  //default listing
  refreshList();
  //refresh
  $interval( function() {

    //console.log("refresh");
    refreshList();

  }, 20000); // 20 seconds


  //ionic push
  /*
  $scope.$on('cloud:push:notification', function(event, data) {
    var msg = data.message;
    alert(msg.title + ': ' + msg.text);
  });*/
  
})
.controller('LoginCtrl', function($scope,$window,$http,$httpExtended,$location,$ionicHistory,sharedToken,$ionicLoading,$ionicPush,AppVersion) {
   $scope.data = {
        username: '',
        password: ''
  }
  $scope.app_version = AppVersion.get();
  //login
  $scope.login = function() {
    $ionicLoading.show({
      template: 'Loading...',
      duration: 3000
    }).then(function(){
       //console.log("The loading indicator is now displayed");
    });
    console.log("Logging in"+$scope.data.username);
    console.log("Logging in"+$scope.data.password);
    $httpExtended.post("https://brmarketplace.com.my/apis/admin_users/login",{
      "login_id" : $scope.data.username ,
      "password" : $scope.data.password
    }).then(function(r){
      console.log(r);
      if(r.data.success){
        $ionicHistory.nextViewOptions({
            historyRoot: true
        });
        var token = r.data.auth_token;
        sharedToken.settoken(token);
        //console.log(r);
        //ionic push
        // $ionicPush.register().then(function(t) {
        //   return $ionicPush.saveToken(t);
        // }).then(function(t) {
        //   console.log('Token saved:', t.token);
        // });
        //get fa id and fa name
        $http.get("https://brmarketplace.com.my/apis/admin_users/me?auth_token="+sharedToken.gettoken()).then(function(r){
            sharedToken.setid(r.data.data.a_id);
            sharedToken.setname(r.data.data.name);
            //console.log(r);
            $window.location.href = "#/tab/dash";
            $ionicLoading.hide().then(function(){
              //console.log("The loading indicator is now hidden");
            });
        });
      }
    });
  };
})
.controller('ChatsCtrl', function($scope,$http,$httpExtended,sharedToken) {
  //console.log(sharedToken.gettoken());
  
  $http.get("https://brmarketplace.com.my/apis/messages/did_communicate?auth_token="+sharedToken.gettoken()).then(function(r){
      //console.log(r);
      $scope.temp = r.data.users.filter(function (request) {
          return (request != false);
      });
      $scope.chatmessage = $scope.temp;
      /*
      $scope.chatmessage = $scope.chats.filter(function (message) {
          return (message.receiver != sharedToken.getid());
      });*/
  });
})

.controller('ChatDetailCtrl', function($scope, $stateParams,$timeout,$ionicScrollDelegate,$http,$httpExtended,sharedToken,$interval) {
  //console.log($stateParams);
  $scope.chatname = $stateParams.name;
  $scope.hideTime = true;
  var alternate,isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();
  $scope.input = { message:""};
  $scope.sendMessage = function() {
    $httpExtended.post("https://brmarketplace.com.my/apis/messages",{
        "auth_token" : sharedToken.gettoken(),
        "receiver_id" : $stateParams.chatId ,
        "message" : $scope.input.message
    }).then(function(r){
        //console.log(r);
        $scope.input.message = "";
        //refresh data
        $http.get("https://brmarketplace.com.my/apis/messages/"+$stateParams.chatId+"?auth_token="+sharedToken.gettoken()).then(function(r){
        $scope.messages = r.data.messages;
        $ionicScrollDelegate.scrollBottom(true);
  });
    });
    delete $scope.data.message;
    //$ionicScrollDelegate.scrollBottom(true);

  };

  $scope.inputUp = function() {
    if (isIOS) $scope.data.keyboardHeight = 216;
    $timeout(function() {
      //$ionicScrollDelegate.scrollBottom(true);
    }, 300);
  };

  $scope.inputDown = function() {
    if (isIOS) $scope.data.keyboardHeight = 0;
    $ionicScrollDelegate.resize();
  };

  $scope.closeKeyboard = function() {
    // cordova.plugins.Keyboard.close();
  };

  //default data
  $scope.data = {};

  $http.get("https://brmarketplace.com.my/apis/messages/"+$stateParams.chatId+"?auth_token="+sharedToken.gettoken()).then(function(r){
      $scope.messages = r.data.messages;
      $scope.myId = sharedToken.getid();
      $ionicScrollDelegate.scrollBottom(true);
      //console.log($scope.messages);
  });

  //refresh
  $interval( function() {
    //console.log("refresh chat");
    $http.get("https://brmarketplace.com.my/apis/messages/"+$stateParams.chatId+"?auth_token="+sharedToken.gettoken()).then(function(r){
        $scope.messages = r.data.messages;
        $scope.myId = sharedToken.getid();
        //$ionicScrollDelegate.scrollBottom(true);
    });
  }, 1000); // 1 seconds
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
  
  $scope.showinfo = function () {
    show(0);
  }
  $scope.showdetails = function () {
    show(1);
  }
  $scope.showbrochure = function () {
   show(2);
  }
  $scope.Goto = function () {
    var link = "https://s3-ap-southeast-1.amazonaws.com/bankrakyat/button/faMikro+MUsk_flyer_v2.pdf";
    //var ref = cordova.InAppBrowser.open(link,"_self","location=yes");
    window.open(link,'_system','location=yes');
    //window.open(link,'_system');
  }
  var show  = function(num) {
    $scope.tab_info = "";
    $scope.tab_details = "";
    $scope.tab_brochure = "";
    $scope.info = false;
    $scope.details = false;
    $scope.brochure = false;
    switch(num){
      case 0:
      $scope.info = true;
      $scope.tab_info = "active";
      break;

      case 1:
      $scope.details = true;
      $scope.tab_details = "active";
      break;

      case 2:
      $scope.brochure = true;
      $scope.tab_brochure = "active";
      break;
    }
  };
  //default
  show(0);
});

