angular.module('starter.services', [])

.service('sharedToken', function () {
    var property = '';
    var id = 0;
    var name = "";
    return {
        gettoken: function () {
            return property;
        },
        settoken: function(value) {
            property = value;
        },
        getid: function () {
            return id;
        },
        setid: function(value) {
            id = value;
        },
        getname: function () {
            return name;
        },
        setname: function(value) {
            name = value;
        }
    };
})
.service('URLManager', [function(){
    return {
        jsonToParams:function(json){
            var str = [];
            for(var j in json)
                str.push(encodeURIComponent(j) + "=" + encodeURIComponent(json[j]));
            return str.join("&");
        }
    }
}])
.service('AppVersion', [function(){
    var code = "";
    return {
        get: function () {
            return code;
        },
        set: function(value) {
            code = value;
        }
    }
}])
.service('$httpExtended', ['$http','URLManager',function($http,URLManager){
    return {
        post:function(url,data){
            return $http({
                        method:"POST",
                        url:url,
                        data: URLManager.jsonToParams(data),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    });
        },
        postJson:function(url,data){
            return $http({
                        method:"POST",
                        url:url,
                        data: data,
                        headers: {'Content-Type': 'application/json'}
                    });
        },
        get:function(url){
            return $http({
                        method:"GET",
                        url:url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    });
        }
    };
}])
